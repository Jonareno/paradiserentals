import java.util.*;
import java.util.Scanner;

public class run {
    static final Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {
        PrintBanner();
        Order myOrder;
        DayOfTheWeek today;
        OrderTypes orderToAdd;
        
        while ( GenerateMenuFromEnum(reader, "\nChoose an action: (enter a number)", MainMenuOptions.class) != MainMenuOptions.Exit){
            myOrder = new Order(reader);
            today = GenerateMenuFromEnum(reader, "\nWhat day of the week is it? (enter a number)", DayOfTheWeek.class);
            
            do {
                orderToAdd = GenerateMenuFromEnum(reader, "\nAdd to this order: (enter a number)", OrderTypes.class);

                switch (orderToAdd) {
                    case Done:
                    break;
                    case MidsizeCar: 
                    myOrder.Add(new MidSizedCar(reader, today));
                    break;
                    case EconomyCar: 
                    myOrder.Add(new EconomyCar(reader, today));
                        break;
                    case SUV: 
                    myOrder.Add(new SUV(reader, today));
                        break;
                    case LuxuryCar:
                    myOrder.Add(new LuxuryCar(reader, today));
                        break;
                    case Limousine:
                    myOrder.Add(new Limousine(reader, today));
                        break;
                    case Bicycle:
                    myOrder.Add(new Bicycle(reader));
                        break;
                    case Scooter:
                    myOrder.Add(new Scooter(reader));
                        break;
                    case Motorcycle:
                    myOrder.Add(new Motorcycle(reader, today));
                        break;
                    case Jetski:
                    myOrder.Add(new Jetski(reader));
                        break;

                }

            } while (orderToAdd != OrderTypes.Done);

            System.out.println(myOrder.Complete());
        }

        reader.close();
        System.out.print("\ngoodbye...\n");        

    }

    private enum MainMenuOptions {
        Exit("Exit"),NewOrder("New Order");

        private String name;

        MainMenuOptions(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    static <E extends Enum<E>>E GenerateMenuFromEnum(Scanner reader, String prompt, Class<E> menuEnum){
        System.out.println(prompt);
        for (E type : menuEnum.getEnumConstants() ){
            System.out.println("\t" + type.ordinal() + ". " + type.toString());
        }
        return menuEnum.getEnumConstants()[reader.nextInt()];
    }

    static void PrintBanner(){
        ClearScreen();
        System.out.println("\n   .-.                             .                 \n  (_) )-.                         /    .-.           \n     /   \\  .-.    ).--..-.  .-../     `-' .    .-. \n    /     )(  |   /    (  | (   /     /   / \\ ./.-'_\n .-/  `--'  `-'-'/      `-'-'`-'-.._.(__./ ._)(__.'  \n(_/                                     /            \n   .-.                                 .       \n  (_) )-.                 /           /        \n     /   \\   .-.  ).-.---/---.-.     /   .    \n    /     )./.-'_ /   ) /   (  |    /   / \\   \n .-/  `--' (__.' /   ( /     `-'-'_/_.-/ ._)   \n(_/     `-._)         `-              /        ");
        System.out.println("\t\t\t\t\tJonathan White");
    }

    static void ClearScreen(){
        System.out.print("\033[H\033[2J");
    }
}