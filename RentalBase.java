import java.util.Scanner;

public class RentalBase implements RentalInterface {
    protected double rate = 0;
    protected Scanner reader;

    RentalBase(Scanner _reader){
        reader = _reader;
    }

    public double GetRate(){
        return rate;
    }
}