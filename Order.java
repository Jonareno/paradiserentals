import java.util.*;

public class Order {
    private class Discounts {
        boolean containsSUV = false;
        boolean containsBicycle = false;
    }
    private List<RentalBase> Rentals;
    protected Scanner reader;
    private final String spacer = "\n---------------------------------------------\n";
    private final String tabbed = "\t\t\t\t\t";

    Order(Scanner _reader){
        reader = _reader;
        Rentals = new ArrayList<RentalBase>();
    }

    public void Add(RentalBase rental){
        Rentals.add(rental);
    }

    public String Complete(){
        double totalCost = 0;
        Discounts discounts = new Discounts();

        for (RentalBase rental : Rentals) {
            totalCost+= rental.GetRate();
            discounts.containsSUV = discounts.containsSUV || rental instanceof SUV;
            discounts.containsBicycle = discounts.containsBicycle || rental instanceof Bicycle;
        }

        totalCost = CheckForDiscounts(totalCost, discounts);
        
        return spacer + "Order total:\n" + tabbed + totalCost + spacer;
    }

    private double CheckForDiscounts(double totalCost, Discounts discounts){
        if (discounts.containsSUV && discounts.containsBicycle){
            double discountValue = new Bicycle(reader).GetRate();
            System.out.println(spacer + "Discount applied! (SUV + Bicycle)");
            System.out.println(tabbed + "-" + discountValue);
            totalCost -= discountValue;
        }

        return totalCost;
    }
    
}

