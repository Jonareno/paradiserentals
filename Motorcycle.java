import java.util.Scanner;

public class Motorcycle extends MidSizedCar {

    private double driverHazardFee = 0;

    Motorcycle(Scanner _reader, DayOfTheWeek day){
        super(_reader, day);
        driverHazardFee = DriverHazardFeeMenu();
    }

    public double GetRate(){
        return (super.GetRate() + driverHazardFee);
    }

    private double DriverHazardFeeMenu() {
        System.out.println("How old is the customer?");
        return GetHazardFee(reader.nextInt());
    }

    private double GetHazardFee(int renterAge){
        double hazardFee;
        if (renterAge >= 18 && renterAge < 26)
            hazardFee = 50;
        else if (renterAge >= 26 && renterAge < 32)
            hazardFee = 35;
        else if (renterAge >= 33 && renterAge < 45)
            hazardFee = 15;
        else
            hazardFee = 0;

        return hazardFee;
    }
    
}