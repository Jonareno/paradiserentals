import java.util.Scanner;

public class EconomyCar extends MidSizedCar {

    private double economyDiscount = 0.5;

    EconomyCar(Scanner _reader, DayOfTheWeek day) {
        super(_reader, day);
    }

    public double GetRate() {
        return (super.GetRate() * economyDiscount);
    }

}