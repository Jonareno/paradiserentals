import java.util.Scanner;

public class SUV extends MidSizedCar {

    private double offRoadFee = 15;
    
    SUV(Scanner reader, DayOfTheWeek day) {
        super(reader, day);
        rate = (super.GetRate() * 1.5) + OffRoadFeeMenu();
    }

    @Override
    public double GetRate(){
        return rate;
    }

    private double OffRoadFeeMenu(){
        System.out.println("Will the SUV be used for offroading? (y/n)");
        return reader.next().charAt(0) == 'y' ? offRoadFee : 0;
    }

}