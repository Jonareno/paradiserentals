LogicManager - Java programming assignment
Developer - Jonathan White

The provided code was writen using Visual Studio Code, and Java v10. To run the code, simply compile and exacute the "run" class. From there you can view the program in your console. 
The program allows users to create new orders, based on the specs outlined in the programming assignment. Orders are for a single day and the order total is printed upon order completion. 
I intentionally omitted tests and input validation since it was not included in the assignment however I'd be happy to discuss how that would be applied to the program. Thank you for your time!