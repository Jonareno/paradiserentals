public enum OrderTypes {
    Done("Done - Complete Order"),
    MidsizeCar("Midsized Car"),
    EconomyCar("Economy Car"),
    SUV("SUV"),
    LuxuryCar("Luxury Car"),
    Limousine("Limousine"),
    Bicycle("Bicycle"),
    Scooter("Scooter"),
    Motorcycle("Motocycle"),
    Jetski("Jetski");

    private String name;

    OrderTypes(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return this.name;
    }
}