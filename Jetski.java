import java.util.Scanner;

public class Jetski extends RentalBase {

    private double hours = 0;

    Jetski(Scanner _reader){
        super(_reader);
        rate = 25;
        hours = AskForHoursMenu();
    }

    @Override
    public double GetRate(){
        return rate * hours;
    }

    private double AskForHoursMenu() {
        System.out.println("How many hours will you have the Jetski?");        
        return reader.nextInt();
    }
    
}