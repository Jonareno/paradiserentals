import java.util.Scanner;

public class MidSizedCar extends RentalBase {

    private DayOfTheWeek today;

    MidSizedCar(Scanner _reader, DayOfTheWeek day){
        super(_reader);
        rate = 30;
        today = day;
    }

    @Override
    public double GetRate(){
        if (today == DayOfTheWeek.Friday || today == DayOfTheWeek.Saturday) {
            return rate * 1.25;
        }
        if (today == DayOfTheWeek.Sunday) {
            return rate * 0.8;
        }
        return rate;
    }
}