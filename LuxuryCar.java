import java.util.Scanner;

public class LuxuryCar extends MidSizedCar {

    private double luxuryPremium = 2;

    LuxuryCar(Scanner reader, DayOfTheWeek day) {
        super(reader, day);
    }

    public double GetRate() {
        return (super.GetRate() * luxuryPremium);
    }

}