import java.util.Scanner;

public class Limousine extends RentalBase {

    private DayOfTheWeek today;
    private double driverRate = 150;
    private double premiumFee = 75;
    private double sundaySpecial = 100;
    private double totalFees = 0;
    private double weekendPremium = 1.4;

    Limousine (Scanner reader, DayOfTheWeek day) {
        super(reader);
        rate = 100;
        today = day;
        if (today == DayOfTheWeek.Sunday) {
            totalFees += AddSundaySpecial();
            // rate = (rate + totalFees);
        } else {
            totalFees += AddDriverMenu() + AddPremiumMenu();
        }
        rate = (rate + totalFees);
        if (today == DayOfTheWeek.Friday || today == DayOfTheWeek.Saturday) {
            rate *= weekendPremium;
        }
    }

    private double AddDriverMenu(){
        System.out.println("Will you need a driver? (y/n)");
        return UserEnteredYes() ? driverRate : 0;
    }

    private double AddPremiumMenu() {
        System.out.println("Would you like the Premium Package? (y/n)");
        return UserEnteredYes() ? premiumFee : 0;
    }

    private double AddSundaySpecial() {
        System.out.println("It's Sunday! Would you like a driver and Premium Pacakge? (y/n)");
        return UserEnteredYes() ? sundaySpecial : 0;
    }

    private boolean UserEnteredYes(){
        return reader.next().charAt(0) == 'y';
    }

}